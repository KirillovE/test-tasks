//
//  Validator.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

/// Определеяет структуру кода, проверяющего строку на соответствие требованиям
protocol Validator {
    
    /// Следующий проверяющий
    var nextValidator: Validator? { get }
    
    /// Правило для проверки
    var rule: (String) -> ValidationResult { get }
    
    /// Проверить на соответствие правилу
    ///
    /// - Parameter candidate: Строка для проверки
    /// - Returns: Результат проверки
    func validate(_ candidate: String) -> ValidationResult
}
