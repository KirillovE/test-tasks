//
//  ValidatorFactory.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

/// Единый шаблон для создания фабрик проверок
protocol ValidatorFactory {
    
    /// Получить проверку
    ///
    /// - Returns: Настроенная проверка
    func getValidator() -> Validator
}
