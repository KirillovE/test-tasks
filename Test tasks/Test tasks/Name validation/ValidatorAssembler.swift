//
//  ValidatorAssembler.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

/// Сборка проверок
struct ValidatorAssembler {
    
    let emailValidator = EmailValidatorFactory().getValidator()
    let nicknameValidator = NicknameValidatorFactory().getValidator()
    
}
