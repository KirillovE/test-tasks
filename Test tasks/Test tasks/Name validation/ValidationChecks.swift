//
//  ValidationChecks.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

/// Результат проверки на соответствие требованиям
enum ValidationResult {
    
    /// Проверка пройдена
    case valid(ValidationCheck)
    
    /// Проверка провалена
    case invalid(ValidationCheck)
}

/// Требования
enum ValidationCheck: String, CustomStringConvertible {
    case email, nickname, length, symbols, firstSymbol
    
    var description: String {
        return self.rawValue
    }
}
