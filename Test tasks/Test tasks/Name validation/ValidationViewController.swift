//
//  ValidationViewController.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 24/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

final class ValidationViewController: UIViewController {

    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var textInput: UITextField!
    private let validatorAssembler = ValidatorAssembler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tapGesture.addTarget(self, action: #selector(hideKeayboard))
    }
    
    @objc private func hideKeayboard() {
        textInput.resignFirstResponder()
    }
    
}

extension ValidationViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        let result = validate(text)
        print(result)
    }
    
}

extension ValidationViewController {
    
    private func validate(_ candidate: String) -> ValidationResult {
        return validateEmail(candidate)
    }
    
    private func validateEmail(_ candidate: String) -> ValidationResult {
        let emailValidator = validatorAssembler.emailValidator
        
        switch emailValidator.validate(candidate) {
        case .valid(let check):
            return .valid(check)
        case .invalid:
            return validateNickname(candidate)
        }
    }
    
    private func validateNickname(_ candidate: String) -> ValidationResult {
        let nicknameValidator = validatorAssembler.nicknameValidator
        
        switch nicknameValidator.validate(candidate) {
        case .valid(let check):
            return .valid(check)
        case .invalid(let check):
            return .invalid(check)
        }
    }
    
}
