//
//  EmailValidatorFactory.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

/// Создание проверки электронного адреса
struct EmailValidatorFactory {
    
    private let target: ValidationCheck = .email
    
    private func getAtSignValidator() -> Validator {
        return TargetedValidator(target: target, nextValidator: getElementsAroundAtSignValidator()) { candidate in
            return candidate.contains("@") ? .valid(.email) : .invalid(.email)
        }
    }
    
    private func getElementsAroundAtSignValidator() -> Validator {
        return TargetedValidator(target: target, nextValidator: nil) { candidate in
            let parts = candidate.split(separator: "@")
            return parts.count == 2 ? .valid(.email) : .invalid(.email)
        }
    }
    
}

extension EmailValidatorFactory: ValidatorFactory {
    
    func getValidator() -> Validator {
        return getAtSignValidator()
    }
    
}
