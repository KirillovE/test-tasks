//
//  TargetedValidator.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

/// Проверка, нацеленная на произвольное требование
struct TargetedValidator: Validator {
    
    let target: ValidationCheck
    let nextValidator: Validator?
    let rule: (String) -> ValidationResult
    
    func validate(_ candidate: String) -> ValidationResult {
        switch rule(candidate) {
        case .valid:
            guard let validator = nextValidator else { return .valid(target) }
            return validator.validate(candidate)
        case .invalid(let check):
            return .invalid(check)
        }
    }
    
}
