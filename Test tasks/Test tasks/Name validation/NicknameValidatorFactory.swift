//
//  NicknameValidatorFactory.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import Foundation

struct NicknameValidatorFactory {
 
    private let target: ValidationCheck = .nickname
    
    private func getFirstSymbolValidator() -> Validator {
        return TargetedValidator(target: target, nextValidator: nil) { candidate in
            guard let first = candidate.first else { return .invalid(.firstSymbol) }
            return (first == "." || first == "-" || first.isNumber) ? .invalid(.firstSymbol) : .valid(.firstSymbol)
        }
    }
    
    private func getSymbolsValidator() -> Validator {
        return TargetedValidator(target: target, nextValidator: getFirstSymbolValidator()) { candidate in
            let candidateSet = CharacterSet(charactersIn: candidate)
            
            let latinAlphabet = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstvuwxyz")
            let allowedSymbols = CharacterSet(charactersIn: "-.")
            let fullSet = latinAlphabet.union(allowedSymbols.union(.decimalDigits))
            
            return candidateSet.isSubset(of: fullSet) ? .valid(.symbols) : .invalid(.symbols)
        }
    }
    
    private func getLengthlValidator() -> Validator {
        return TargetedValidator(target: target, nextValidator: getSymbolsValidator()) { candidate in
            let length = candidate.count
            return (length > 2 && length < 33) ? .valid(.length) : .invalid(.length)
        }
    }
    
}

extension NicknameValidatorFactory: ValidatorFactory {
    
    func getValidator() -> Validator {
        return getLengthlValidator()
    }
    
}
