//
//  ImagesViewController.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 24/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

final class ImagesViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    private let reuseId = "TableCell"
    private let baseAddress = "http://placehold.it/375x150?text="
    
    private lazy var links: [URL] = {
        return Array(1...100)
            .map { baseAddress + String($0) }
            .compactMap { URL(string: $0) }
    }()
    
    private var images = [URL: UIImage?]() {
        didSet {
            DispatchQueue.main.async {
                self.table.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchImages()
    }
    
    private func fetchImages() {
        let cell = UITableViewCell()
        links.forEach { downloadImage(withURL: $0, forCell: cell) }
    }

    private func downloadImage(withURL url: URL, forCell cell: UITableViewCell) {
        DispatchQueue.global().async {
            let imageData = try? Data(contentsOf: url)
            self.images[url] = imageData.flatMap { UIImage(data: $0) }
        }
    }
    
}

extension ImagesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return links.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseId,
                                                       for: indexPath) as? ImagesTableViewCell
            else { return UITableViewCell() }
        
        let address = links[indexPath.row]
        if let image = images[address] {
            cell.configure(withImage: image)
        } else {
            cell.configure(withImage: #imageLiteral(resourceName: "placeholder"))
        }
        
        return cell
    }
    
}

extension ImagesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let link = links[indexPath.row]
        let cell = UITableViewCell()
        downloadImage(withURL: link, forCell: cell)
    }
    
}
