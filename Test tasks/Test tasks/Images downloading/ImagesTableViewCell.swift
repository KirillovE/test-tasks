//
//  ImagesTableViewCell.swift
//  Test tasks
//
//  Created by Евгений Кириллов on 25/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import UIKit

final class ImagesTableViewCell: UITableViewCell {

    @IBOutlet weak var placeholderImage: UIImageView!
    
    func configure(withImage image: UIImage?) {
        placeholderImage.image = image
    }
    
}
