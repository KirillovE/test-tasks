//
//  EmailValidatorTests.swift
//  Test tasksTests
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import XCTest
@testable import Test_tasks

class EmailValidatorTests: XCTestCase {

    let validator = EmailValidatorFactory().getValidator()

    func testSuccess() {
        let name = "some@example"
        let result = validator.validate(name)
        
        switch result {
        case .invalid:
            XCTFail()
        default :
            break
        }
    }
    
    func testAtSignFailure() {
        let name = "some_example"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default :
            break
        }
    }
    
    func testEmptyLeftFailure() {
        let name = "some@"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default :
            break
        }
    }
    
    func testEmptyRightFailure() {
        let name = "@example"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default :
            break
        }
    }
    
    func testEmptyLeftRightFailure() {
        let name = "@"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default :
            break
        }
    }

}
