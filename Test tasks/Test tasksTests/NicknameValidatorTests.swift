//
//  NicknameValidatorTests.swift
//  Test tasksTests
//
//  Created by Евгений Кириллов on 29/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import XCTest
@testable import Test_tasks

class NicknameValidatorTests: XCTestCase {

    let validator = NicknameValidatorFactory().getValidator()

    // MARK: - First symbol
    
    func testFirstSymbolSuccess() {
        let name = "some"
        let result = validator.validate(name)
        
        switch result {
        case .invalid:
            XCTFail()
        default:
            break
        }
    }
    
    func testFirstSymbolNumberFailure() {
        let name = "1some"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default:
            break
        }
    }
    
    func testFirstSymbolDotFailure() {
        let name = ".some"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default:
            break
        }
    }
    
    func testFirstSymbolMinusFailure() {
        let name = "-some"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default:
            break
        }
    }
    
    // MARK: - Symbols
    
    func testSymbolsSuccess() {
        let name = "some"
        let result = validator.validate(name)
        
        switch result {
        case .invalid:
            XCTFail()
        default:
            break
        }
    }
    
    func testSymbolsRussianFailure() {
        let name = "некто"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default:
            break
        }
    }
    
    func testSymbolsEmojiFailure() {
        let name = "🤩"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default:
            break
        }
    }
    
    // MARK: - Length
    
    func testLengthSuccess() {
        let name = "some"
        let result = validator.validate(name)
        
        switch result {
        case .invalid:
            XCTFail()
        default:
            break
        }
    }
    
    func testLengthSmallFailure() {
        let name = "so"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default:
            break
        }
    }
    
    func testLengthBigFailure() {
        let name = "ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstvuxyz"
        let result = validator.validate(name)
        
        switch result {
        case .valid:
            XCTFail()
        default:
            break
        }
    }

}
