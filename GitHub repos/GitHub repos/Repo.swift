//
//  Repo.swift
//  GitHub repos
//
//  Created by Евгений Кириллов on 28/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

/// Хранилище из GitHub
struct Repo: Decodable {
    
    /// Идентификатор хранилища
    let id: Int
    
    /// Название хранилища
    let name: String
    
    /// Полное название хранилища
    let fullName: String
}
