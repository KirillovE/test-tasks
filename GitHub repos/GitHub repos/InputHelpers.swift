//
//  InputHelpers.swift
//  GitHub repos
//
//  Created by Евгений Кириллов on 28/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

/// Работа с пользовательским вводом
struct InputHelpers {
    
    /// Получить от пользователя непустую строку
    ///
    /// - Parameter invitation: Приглашение для пользовательского ввода
    /// - Returns: Результат ввода
    func getNotEmptyInput(withInvitation invitation: String) -> String {
        repeat {
            print(invitation, terminator: " ")
            if let input = readLine(), !input.isEmpty {
                return input
            }
        } while true
    }
    
}
