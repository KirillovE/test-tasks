//
//  Network.swift
//  GitHub repos
//
//  Created by Евгений Кириллов on 28/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import Alamofire

/// Работа с сетью
struct Network {
    
    private let base = "https://api.github.com/users/"
    private let method = "/repos"
    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    /// Получить список хранилищ
    ///
    /// - Parameters:
    ///   - user: Имя пользователя GitHub
    ///   - completion: Массив хранилищ пользователя
    func fetchRepos(ofUser user: String, completion: @escaping ([String]) -> Void) {
        guard let address = getURL(forUser: user) else { return completion([]) }
        
        request(address).responseData { response in
            switch response.result {
            case .success(let receivedData):
                let repoNames = self.parseData(receivedData)
                completion(repoNames)
            case .failure(let error):
                print(error.localizedDescription)
                completion([])
            }
        }
    }
    
    private func getURL(forUser user: String) -> URL? {
        let link = base + user + method
        return URL(string: link)
    }
    
    private func parseData(_ data: Data) -> [String] {
        do {
            let repos = try self.decoder.decode([Repo].self, from: data)
            return repos.map { $0.name }
        } catch {
            print(error.localizedDescription)
            return []
        }
    }
    
}
