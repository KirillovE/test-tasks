//
//  main.swift
//  GitHub repos
//
//  Created by Евгений Кириллов on 28/04/2019.
//  Copyright © 2019 Триада. All rights reserved.
//

import Foundation

var taskComplete = false

let username = InputHelpers().getNotEmptyInput(withInvitation: "Имя пользователя GitHub:")

Network().fetchRepos(ofUser: username) { repoNames in
    print("\n--- Хранилища пользователя \(username) ---")
    
    repoNames.enumerated().forEach { index, value in
        print("#\(index + 1): \(value)")
    }
    
    print("--- Количество хранилищ: \(repoNames.count) ---\n")
    taskComplete = true
}

while !taskComplete {
    RunLoop.current.run(until: Date() + 1)
}
